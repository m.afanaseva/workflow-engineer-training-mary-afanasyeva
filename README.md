# workflow-engineer-training-mary-afanasyeva

## Личные сведения 

Меня зовут Маша Афанасьева, я учусь на 3 курсе БГУИР на специальности АСОИ. В свободное время люблю читать книги и смотреть сериалы.

## Оценка навыков

| Навык | Оценка |
|:-----:|:------:|
|python |3       |
|mysql  |2       |
|yaml   |1       |
|csv    |2       |
|md     |1       |
|git    |2       |
|redmine|0       |
|jira   |0       |


## Время, когда я доступна


| День       | Время        |
|:----------:|:------------:|
|понедельник |9:00 - 17:00  |
|вторник     |9:00 - 17:00  |
|среда       |10:00 - 18:00 |
|четверг     |13:00 - 21:00 |
|пятница     |8:00 - 16:00  |

## Основные ссылки 

+ [GitHub](https://github.com/MariaAfanasyeva)
+ [GitLab](https://gitlab.com/m.afanaseva)
+ [Jira]( https://workflow-engineers-syberry.atlassian.net/jira/people/604ba5ae3ab18c0068fa3398)
+ [Redmine](https://www.redmine.org/users/512456)
+ [Discord](Masha_Afanaseva#8561)

